import pygame
from random import randint

pygame.init()


screen = pygame.display.set_mode((800,800))
pygame.display.set_caption("Target Practice by Greg Cabrera")

buttonArea = pygame.Rect(200, 200, 100, 100)

lime=(52,205,50)
size = randint(10, 15)
position = (randint(60, 780),randint(60, 780))
area = pygame.draw.circle(screen, lime, position, size)

myfont = pygame.font.SysFont("monospace", 15)
userScore = myfont.render("Score: {}".format(0), 1, (255,255,255))

clock = pygame.time.Clock()
counter, timer = 60, '60'.rjust(3)
pygame.time.set_timer(pygame.USEREVENT, 1000)

score = 0
run = True

while run == True:
	for event in pygame.event.get():
		if event.type == pygame.USEREVENT: 
			counter -= 1
			timer = str(counter).rjust(3) if counter > 0 else "0" 

		if event.type == pygame.QUIT:
			run = False

		if event.type == pygame.MOUSEBUTTONDOWN:
			if event.button == 1:
				if area.collidepoint(event.pos):
						size = randint(10, 15)
						position = (randint(60, 780),randint(60, 780))
						area = pygame.draw.circle(screen, lime, position, size)
						score = score + 1
						userScore = myfont.render("Score: {}".format(score), 1, (255,255,255))

				
	screen.fill((0, 0, 0))
	screen.blit(userScore, (700,25))
	screen.blit(myfont.render("Time: {}".format(timer), True, (255,255,255)), (600,25))
	pygame.draw.circle(screen, lime, position, size)
	clock.tick(60)
	if counter == 0:
		score = 0
		counter = 61
	pygame.display.flip()

		

pygame.display.quit()
pygame.quit()
sys.exit()

