VERSIONS

v0
- Added game.py
- rectangle with simple movement from user key press.

v1
- Changed idea to practice shooting concept.
- Imlpemented collision on mouse click.
- Re-draws rectangle when clicked.
- Scoring system.

v2
- Added timer.
- Changed shape to circle.
- Fixed position render.

v3
- Game restarts after time is 0